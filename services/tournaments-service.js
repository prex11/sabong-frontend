import qs from "qs";
import Jsona from "jsona";

const url = process.env.apiUrl;
const jsona = new Jsona();

function list(params, axios) {
  const options = {
    params: params,
    paramsSerializer: function (params) {
      return qs.stringify(params, { encode: false });
    },
  };
  return axios.get(`${url}/tournaments`, options).then((response) => {
    return {
      list: jsona.deserialize(response.data),
      meta: response.data.meta,
    };
  });
}

function get(id, axios) {
  return axios.get(`${url}/tournaments/${id}`).then((response) => {
    let tournament = jsona.deserialize(response.data);
    delete tournament.links;
    return tournament;
  });
}

function add(tournament, axios) {
  const payload = jsona.serialize({
    stuff: tournament,
    includeNames: null,
  });

  return axios.post(`${url}/tournaments`, payload).then((response) => {
    return jsona.deserialize(response.data);
  });
}

function update(tournament, axios) {
  const payload = jsona.serialize({
    stuff: tournament,
    includeNames: [],
  });

  return axios
    .patch(`${url}/tournaments/${tournament.id}`, payload)
    .then((response) => {
      return jsona.deserialize(response.data);
    });
}

function destroy(id, axios) {
  return axios.delete(`${url}/tournaments/${id}`);
}

export default {
  list,
  get,
  add,
  update,
  destroy,
};
