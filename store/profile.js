import service from "../services/profile-service";

export const state = () => ({
  me: null,
  credit: 0,
});

export const mutations = {
  SET_RESOURCE: (state, me) => {
    state.me = me;
  },
  SET_CREDIT: (state, credit) => {
    state.credit = credit;
  },
};

export const actions = {
  me({ commit, dispatch }, params) {
    return service.get(params, this.$axios).then((profile) => {
      commit("SET_RESOURCE", profile.list);
      commit("SET_CREDIT", profile.list.credit);
    });
  },

  setCredit({ commit, dispatch }, credit) {
    commit("SET_CREDIT", credit);
  },

  update({ commit, dispatch }, params) {
    return service.update(params, this.$axios).then((profile) => {
      commit("SET_RESOURCE", profile);
    });
  },
};

export const getters = {
  me: (state) => state.me,
  credit: (state) => state.credit,
};
