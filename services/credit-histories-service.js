import qs from "qs";
import Jsona from "jsona";

const url = process.env.apiUrl;
const jsona = new Jsona();

function list(params, axios) {
  const options = {
    params: params,
    paramsSerializer: function (params) {
      return qs.stringify(params, { encode: false });
    },
  };
  return axios.get(`${url}/credit-histories`, options).then((response) => {
    return {
      list: jsona.deserialize(response.data),
      meta: response.data.meta,
    };
  });
}

function get(id, axios) {
  return axios.get(`${url}/credit-histories/${id}`).then((response) => {
    let credit_history = jsona.deserialize(response.data);
    delete credit_history.links;
    return credit_history;
  });
}

function add(credit_history, axios) {
  const payload = jsona.serialize({
    stuff: credit_history,
    includeNames: null,
  });

  return axios.post(`${url}/credit-histories`, payload).then((response) => {
    return jsona.deserialize(response.data);
  });
}

function update(credit_history, axios) {
  const payload = jsona.serialize({
    stuff: credit_history,
    includeNames: [],
  });

  return axios
    .patch(`${url}/credit-histories/${credit_history.id}`, payload)
    .then((response) => {
      return jsona.deserialize(response.data);
    });
}

function destroy(id, axios) {
  return axios.delete(`${url}/credit-histories/${id}`);
}

export default {
  list,
  get,
  add,
  update,
  destroy,
};
