import service from "../services/bettings-service";

export const state = () => ({
  list: {},
  betting: {},
  meta: {},
});

export const mutations = {
  SET_LIST: (state, list) => {
    state.list = list;
  },
  SET_RESOURCE: (state, betting) => {
    state.betting = betting;
  },
  SET_META: (state, meta) => {
    state.meta = meta;
  },
};

export const actions = {
  list({ commit, dispatch }, params) {
    return service.list(params, this.$axios).then(({ list, meta }) => {
      commit("SET_LIST", list);
      commit("SET_META", meta);
    });
  },

  get({ commit, dispatch }, params) {
    return service.get(params, this.$axios).then((betting) => {
      commit("SET_RESOURCE", betting);
    });
  },

  add({ commit, dispatch }, params) {
    return service.add(params, this.$axios).then((betting) => {
      commit("SET_RESOURCE", betting);
    });
  },

  update({ commit, dispatch }, params) {
    return service.update(params, this.$axios).then((betting) => {
      commit("SET_RESOURCE", betting);
    });
  },

  placeBet({ commit, dispatch }, params) {
    return service.placeBet(params, this.$axios).then((bettings) => {
      // commit("SET_RESOURCE", betting);
      return bettings
    });
  },

  placeBetMashador({ commit, dispatch }, params) {
    return service.placeBetMashador(params, this.$axios).then((betting) => {
      // commit("SET_RESOURCE", betting);
      return betting
    });
  },

  destroy({ commit, dispatch }, params) {
    return service.destroy(params, this.$axios);
  },
};

export const getters = {
  list: (state) => state.list,
  listTotal: (state) => state.meta.page.total,
  betting: (state) => state.betting,
  dropdown: (state) => {
    return state.list.map((betting) => ({
      id: betting.id,
      name: betting.betting_number,
    }));
  },
};
