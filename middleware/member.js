export default function ({ app, redirect }) {
  if (app.$auth.loggedIn && app.$auth.user.roles[0].name !== 'member') {
    let roles = app.$auth.user.roles.map(item => {
      return item.name
    })

    if (roles.includes('admin'))
      return redirect('/tournaments')
    if (roles.includes('member'))
      return redirect('/user-profile')
    if (roles.includes('creator'))
        return redirect('/users/add-credit')
  }
}