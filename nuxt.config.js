/*!

=========================================================
* Nuxt Argon Dashboard PRO Laravel- v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/nuxt-argon-dashboard-pro-laravel
* Copyright Creative Tim (https://www.creative-tim.com) & UPDIVISION (https://www.updivision.com)

* Coded by www.creative-tim.com & www.binarcode.com & www.updivision.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

const { setInteractionMode } = require("vee-validate");
const pkg = require("./package");
const { default: isDemo } = require("./plugins/isDemo");
const  { default: baseUrl } = require("./plugins/baseUrl");

module.exports = {
  env: {
    apiUrl: process.env.API_BASE_URL,
    baseUrl: process.env.BASE_URL,
    isDemo: process.env.IS_DEMO,
    apiKey: process.env.API_KEY,
  },
  mode: "spa",
  router: {
    base: "/",
    linkExactActiveClass: "active",
  },
  meta: {
    ogType: false,
    ogDescription: false,
    author: false,
    ogTitle: false,
    description: false,
    viewport: false,
    charset: false,
  },
  /*
   ** Headers of the page
   */
  head: {
    title: "Sabong ni Sabas",
    meta: [
      { charset: "utf-8" },
      {
        name: "keywords",
        content:
          "Betting game for fighting rooster.",
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      }
    ],
    link: [
      { rel: "icon", type: "image/png", href: "/favicon.png" },
      // {
      //   rel: "stylesheet",
      //   href:
      //     "https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700",
      // },
      // {
      //   rel: "stylesheet",
      //   href: "https://use.fontawesome.com/releases/v5.6.3/css/all.css",
      //   integrity:
      //     "sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/",
      //   crossorigin: "anonymous",
      // },
    ],
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff", height: '4px' },

  /*
   ** Global CSS
   */
  css: [
    "assets/css/nucleo/css/nucleo.css",
    "assets/css/fontawesome/css/all.css",
    "assets/sass/argon.scss",
    "~assets/css/style.css",
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "~/plugins/dashboard/dashboard-plugin",
    { src: "~/plugins/dashboard/full-calendar", ssr: false },
    { src: "~/plugins/dashboard/world-map", ssr: false },
    "~/plugins/dashboard/JsonApi.js",
    "~/plugins/isDemo.js",
    "~/plugins/baseUrl.js",
    '~/plugins/htmlToPaper.js'
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    "@nuxtjs/auth",
    "@nuxtjs/toast",
  ],

  buildModules: [
    ['@nuxtjs/laravel-echo', {
      broadcaster: 'pusher',
      key: 'SABASKEY',
      wsHost: process.env.LIVE_IP,
      wsPort: 6001,
      forceTLS: false,
      disableStats: true,
    }]
  ],
  /*
   ** Auth module configuration
   ** See https://auth.nuxtjs.org/schemes/local.html#options
   */
  auth: {
    strategies: {
      local: {
        _scheme: "~/util/authCustomStrategy.js",
        endpoints: {
          login: {
            url: "login",
            method: "post",
            propertyName: "access_token",
          },
          logout: { url: "/logout", method: "post" },
          user: {
            url: "/me?include=roles.permissions",
            method: "get",
            propertyName: false,
          },
        },
      },
      redirect: {
        login: "/login",
        logout: "/",
        callback: "/login",
        home: "/tournaments",
      },
    },
  },

  router: {
    middleware: ["auth"],
  },

  /*
   ** Notification toast module configuration
   ** See https://github.com/nuxt-community/modules/tree/master/packages/toast?ref=madewithvuejs.com
   */
  toast: {
    position: "top-right",
    duration: 5000,
    keepOnHover: true,
    fullWidth: false,
    fitToScreen: true,
    className: "vue-toast-custom",
    closeOnSwipe: true,
    register: [
      // Register custom toasts
      // @todo add custom messages as they come : login failed, register failed etc
      {
        name: "my-error",
        message: "Oops...Something went wrong",
        options: {
          type: "error",
        },
      },
    ],
  },

  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.API_BASE_URL,
    headers: {
      common: {
        Accept: "application/vnd.api+json",
        "content-type": "application/vnd.api+json",
      },
      post: {
        "content-type": "application/vnd.api+json",
      },
      patch: {
        "content-type": "application/vnd.api+json",
      },
      delete: {
        "content-type": "application/vnd.api+json",
      },
    },
  },

  /*
   ** Build configuration
   */
  build: {
    transpile: ["vee-validate/dist/rules"],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/,
          options: {
            fix: true,
          },
        });
      }
    },
    extractCSS: process.env.NODE_ENV === "production",
    babel: {
      plugins: [
        [
          "component",
          {
            libraryName: "element-ui",
            styleLibraryName: "theme-chalk",
          },
        ],
      ],
    },
  },

  generate: {
    fallback: true
  }
};
