import service from "../services/fights-service";

export const state = () => ({
  list: {},
  fight: {},
  meta: {},
  meron_url: null,
  wala_url: null,
});

export const mutations = {
  SET_LIST: (state, list) => {
    state.list = list;
  },
  SET_RESOURCE: (state, fight) => {
    state.fight = fight;
  },
  SET_META: (state, meta) => {
    state.meta = meta;
  },
  SET_MERON_URL: (state, url) => {
    state.meron_url = url;
  },
  SET_WALA_URL: (state, url) => {
    state.wala_url = url;
  },
};

export const actions = {
  list({ commit, dispatch }, params) {
    return service.list(params, this.$axios).then(({ list, meta }) => {
      commit("SET_LIST", list);
      commit("SET_META", meta);
    });
  },

  get({ commit, dispatch }, params) {
    return service.get(params, this.$axios).then((fight) => {
      commit("SET_RESOURCE", fight);
    });
  },

  add({ commit, dispatch }, params) {
    return service.add(params, this.$axios).then((fight) => {
      commit("SET_RESOURCE", fight);
    });
  },

  update({ commit, dispatch }, params) {
    return service.update(params, this.$axios).then((fight) => {
      commit("SET_RESOURCE", fight);
    });
  },

  updateStatus({ commit, dispatch }, params) {
    return service.updateStatus(params, this.$axios).then((fight) => {
      // commit("SET_RESOURCE", fight);
    });
  },

  startTimerToFinishFight({ commit, dispatch }, params) {
    return service.startTimerToFinishFight(params, this.$axios).then((fight) => {
      // commit("SET_RESOURCE", fight);
    });
  },

  finishFight({ commit, dispatch }, params) {
    return service.finishFight(params, this.$axios).then((fight) => {
      // commit("SET_RESOURCE", fight);
    });
  },

  undoFight({ commit, dispatch }, params) {
    return service.undoFight(params, this.$axios).then((fight) => {
      // commit("SET_RESOURCE", fight);
    });
  },

  cancelFight({ commit, dispatch }, params) {
    return service.cancelFight(params, this.$axios).then((fight) => {
      // commit("SET_RESOURCE", fight);
    });
  },

  destroy({ commit, dispatch }, params) {
    return service.destroy(params, this.$axios);
  },

  upload({ commit, dispatch }, { image, axios, side }) {
    return service.upload(image, axios).then((url) => {
      if(side == "meron") {
        commit("SET_MERON_URL", url);
      } else {
        commit("SET_WALA_URL", url);
      }
      
    });
  },

  getCurrentFight({ commit, dispatch }, params) {
    return service.getCurrentFight(params, this.$axios).then((fight) => {
      let result = (fight.list) ? fight.list : null
      commit("SET_RESOURCE", result);
    });
  },

  getFightsBettedByUser({ commit, dispatch }, params) {
    return service.getFightsBettedByUser(params, this.$axios).then(({ list, meta }) => {
      commit("SET_LIST", list);
      commit("SET_META", meta);
    });
  },
};

export const getters = {
  list: (state) => state.list,
  listTotal: (state) => state.meta.page.total,
  fight: (state) => state.fight,
  wala_url: (state) => state.wala_url,
  meron_url: (state) => state.meron_url,
  dropdown: (state) => {
    return state.list.map((fight) => ({
      id: fight.id,
      name: fight.fight_number,
    }));
  },
};
