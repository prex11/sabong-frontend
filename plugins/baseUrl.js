export default ({ app }, inject) => {
  const init = process.env.baseUrl || 0;
  inject("baseUrl", init);
};
