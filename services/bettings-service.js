import qs from "qs";
import Jsona from "jsona";

const url = process.env.apiUrl;
const jsona = new Jsona();

function list(params, axios) {
  const options = {
    params: params,
    paramsSerializer: function (params) {
      return qs.stringify(params, { encode: false });
    },
  };
  return axios.get(`${url}/bettings`, options).then((response) => {
    return {
      list: jsona.deserialize(response.data),
      meta: response.data.meta,
    };
  });
}

function get(id, axios) {
  return axios.get(`${url}/bettings/${id}?include=tournament`).then((response) => {
    let betting = jsona.deserialize(response.data);
    delete betting.links;
    return betting;
  });
}

function add(betting, axios) {
  const payload = jsona.serialize({
    stuff: betting,
    includeNames: null,
  });

  return axios.post(`${url}/bettings`, payload).then((response) => {
    return jsona.deserialize(response.data);
  });
}

function update(betting, axios) {
  const payload = jsona.serialize({
    stuff: betting,
    includeNames: [],
  });

  return axios
    .patch(`${url}/bettings/${betting.id}`, payload)
    .then((response) => {
      return jsona.deserialize(response.data);
    });
}

function destroy(id, axios) {
  return axios.delete(`${url}/bettings/${id}`);
}

function placeBet(betting, axios) {
  const payload = jsona.serialize({
    stuff: betting,
    includeNames: null,
  });

  return axios.post(`${url}/place-bet`, payload).then((response) => {
    return response.data
  });
}

function placeBetMashador(betting, axios) {
  const payload = jsona.serialize({
    stuff: betting,
    includeNames: null,
  });

  return axios.post(`${url}/place-bet-mashador`, payload).then((response) => {
    return response.data
  });
}

export default {
  list,
  get,
  add,
  update,
  destroy,
  placeBet,
  placeBetMashador
};
