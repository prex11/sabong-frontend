import service from "../services/credit-histories-service";

export const state = () => ({
  list: {},
  credit_history: {},
  meta: {},
});

export const mutations = {
  SET_LIST: (state, list) => {
    state.list = list;
  },
  SET_RESOURCE: (state, credit_history) => {
    state.credit_history = credit_history;
  },
  SET_META: (state, meta) => {
    state.meta = meta;
  },
};

export const actions = {
  list({ commit, dispatch }, params) {
    return service.list(params, this.$axios).then(({ list, meta }) => {
      commit("SET_LIST", list);
      commit("SET_META", meta);
    });
  },

  get({ commit, dispatch }, params) {
    return service.get(params, this.$axios).then((credit_history) => {
      commit("SET_RESOURCE", credit_history);
    });
  },

  add({ commit, dispatch }, params) {
    return service.add(params, this.$axios).then((credit_history) => {
      commit("SET_RESOURCE", credit_history);
    });
  },

  update({ commit, dispatch }, params) {
    return service.update(params, this.$axios).then((credit_history) => {
      commit("SET_RESOURCE", credit_history);
    });
  },

  destroy({ commit, dispatch }, params) {
    return service.destroy(params, this.$axios);
  },
};

export const getters = {
  list: (state) => state.list,
  listTotal: (state) => state.meta.page.total,
  credit_history: (state) => state.credit_history,
  dropdown: (state) => {
    return state.list.map((credit_history) => ({
      id: credit_history.id,
      name: credit_history.name,
    }));
  },
};
