// For iOS >15 video needs to be on the document to work as a wake lock add to line 45 on index.js nosleep package
Object.assign(this.noSleepVideo.style, {
  position: "absolute",
  left: "-100%",
  top: "-100%",
});
document.querySelector("body").append(this.noSleepVideo);