export default function ({ app, redirect }) {
  if (app.$auth.loggedIn && app.$auth.user.roles[0].name !== 'admin' && app.$auth.user.roles[0].name !== 'creator') {
    let roles = app.$auth.user.roles.map(item => {
      return item.name
    })
    
    if (roles.includes('mashador'))
      return redirect('/user-profile')
    if (roles.includes('member'))
      return redirect('/user-profile')
  }
}