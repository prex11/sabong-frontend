import qs from "qs";
import Jsona from "jsona";

const url = process.env.apiUrl;
const jsona = new Jsona();

function list(params, axios) {
  const options = {
    params: params,
    paramsSerializer: function (params) {
      return qs.stringify(params, { encode: false });
    },
  };
  return axios.get(`${url}/fights`, options).then((response) => {
    return {
      list: jsona.deserialize(response.data),
      meta: response.data.meta,
    };
  });
}

function get(id, axios) {
  return axios.get(`${url}/fights/${id}?include=tournament`).then((response) => {
    let fight = jsona.deserialize(response.data);
    delete fight.links;
    return fight;
  });
}

function add(fight, axios) {
  const payload = jsona.serialize({
    stuff: fight,
    includeNames: null,
  });

  return axios.post(`${url}/fights`, payload).then((response) => {
    return jsona.deserialize(response.data);
  });
}

function update(fight, axios) {
  const payload = jsona.serialize({
    stuff: fight,
    includeNames: [],
  });

  return axios
    .patch(`${url}/fights/${fight.id}`, payload)
    .then((response) => {
      return jsona.deserialize(response.data);
    });
}

function updateStatus(fight, axios) {
  const payload = jsona.serialize({
    stuff: fight,
    includeNames: [],
  });

  return axios
    .patch(`${url}/fight-update-status/${fight.id}`, payload)
    .then((response) => {
      // return jsona.deserialize(response.data);
    });
}

function startTimerToFinishFight(fight, axios) {
  const payload = jsona.serialize({
    stuff: fight,
    includeNames: [],
  });

  return axios
    .patch(`${url}/start-timer-to-finish-fight/${fight.id}`, payload)
    .then((response) => {
      // return jsona.deserialize(response.data);
    });
}

function finishFight(fight, axios) {
  const payload = jsona.serialize({
    stuff: fight,
    includeNames: [],
  });

  return axios
    .patch(`${url}/finish-fight/${fight.id}`, payload)
    .then((response) => {
      // return jsona.deserialize(response.data);
      // console.log(response.data)
    });
}

function undoFight(fight, axios) {
  const payload = jsona.serialize({
    stuff: fight,
    includeNames: [],
  });

  return axios
    .patch(`${url}/undo-fight/${fight.id}`, payload)
    .then((response) => {
      // return jsona.deserialize(response.data);
      // console.log(response.data)
    });
}

function cancelFight(fight, axios) {
  const payload = jsona.serialize({
    stuff: fight,
    includeNames: [],
  });

  return axios
    .patch(`${url}/cancel-fight/${fight.id}`, payload)
    .then((response) => {
      // return jsona.deserialize(response.data);
      // console.log(response.data)
    });
}

function destroy(id, axios) {
  return axios.delete(`${url}/fights/${id}`);
}

function upload(image, nuxt_axios) {
  const payload = new FormData();
  payload.append("attachment", image);

  let axios = nuxt_axios.create();
  delete axios.defaults.headers.common["content-type"];
  delete axios.defaults.headers.post["content-type"];

  return axios({
    method: "POST",
    url: `/fight-uploads/fights/fight-image`,
    data: payload,
    headers: {
      Accept: "application/vnd.api+json",
      "Content-Type": "application/vnd.api+json",
    },
  }).then((response) => {
    return response.data.url;
  });
}

function getCurrentFight(params, axios) {
  return axios.get(`${url}/get-current-fight?include=tournament`).then((response) => {
    let data = {}
    if(response.data && response.data.message === 'no on-going fight.') {
      data = null
    } else {
      data = jsona.deserialize(response.data)
    }
    return {
      list: data,
      // meta: response.data.meta,
    };
  });
}

function getFightsBettedByUser(params, axios) {
  const options = {
    params: params,
    paramsSerializer: function (params) {
      return qs.stringify(params, { encode: false });
    },
  };
  return axios.get(`${url}/get-fights-betted-by-user`, options).then((response) => {
    return {
      list: jsona.deserialize(response.data),
      meta: response.data.meta,
    };
  });
}

export default {
  list,
  get,
  add,
  update,
  destroy,
  updateStatus,
  startTimerToFinishFight,
  finishFight,
  cancelFight,
  upload,
  getCurrentFight,
  getFightsBettedByUser,
  undoFight
};
